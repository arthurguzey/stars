package com.java.stars;

public class Stars {
    int[][] arr= new int[6][6];
    int arrLenght = 7;
    public void drawFirstPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                System.out.print("* ");
            }
            System.out.println();
        }
        System.out.println();
    }
    public void drawSecondPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if ((i==0 || i==6)||(j==0 || j==6))
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }

    public void drawThirdPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if (i==0 || j==0 || (j+i)==6)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }


    public void drawFourthlyPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if (i==6 || j==0 || j==i)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }


    public void drawFifthPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if (i==6 || j==6 || (j+i)==6)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }




    public void drawSixthPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if (i==0 || j==6 || j==i)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }



    public void drawSeventhPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if ((j+i)==6 || j==i)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }


    public void drawEighthPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if ((((j+i)==6 || j==i) && i<=arrLenght/2) || i==0)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }


    public void drawNinthPainting(){
        System.out.println();
        for(int i=0;i<arrLenght;i++){
            for (int j=0;j<arrLenght;j++)
            {
                if ((((j+i)==6 || j==i) && i>=arrLenght/2) || i==6)
                {
                    System.out.print("* ");
                }
                else
                {
                    System.out.print("  ");
                }

            }
            System.out.println();
        }
        System.out.println();
    }
}
