import com.java.stars.Stars;

public class Main {
    public static void main(String[] args) {
        Stars stars=new Stars();
        stars.drawFirstPainting();
        stars.drawSecondPainting();
        stars.drawThirdPainting();
        stars.drawFourthlyPainting();
        stars.drawFifthPainting();
        stars.drawSixthPainting();
        stars.drawSeventhPainting();
        stars.drawEighthPainting();
        stars.drawNinthPainting();
    }
}
